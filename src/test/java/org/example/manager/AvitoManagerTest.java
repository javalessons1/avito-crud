package org.example.manager;
import org.example.data.Apartment;
import org.example.dto.Request;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import org.example.exeption.ApartmentNotFoundException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AvitoManagerTest {

  static String [] requestedRoomAmount1 = {"fiveAndMore"};
  static String [] requestedRoomAmount2 = {"Studio", "freePlanned", "1", "2", "3", "4", "fiveAndMore"};

  static AvitoManager manager = new AvitoManager();

  static Apartment apartmentFiveAndMoreHasLoggia = new Apartment(0, "fiveAndMore", 3_000_000, 85, true, false, 4, 5);
  static Apartment apartment3LastFloor = new Apartment(1, "3", 4_000_000,  64, true, true, 9, 9);
  static Apartment apartmentFirstFloor = new Apartment(2, "freePlanned", 1_000_000, 72, false, false, 1, 15);
  static Apartment apartmentDoesNotExists = new Apartment(3, "Studio", 1_000_000, 72, false, false, 5, 9);

  static Request requestSearchMultiple = new Request(requestedRoomAmount2, 3_000_000, 5_000_000, 30, null, true, false,
          null, null, null, null, false, false);
  static Request requestSearchOne = new Request(requestedRoomAmount1, 3_000_000, 6_000_000, 30, null, false, false,
          null, null, null, null, false, true);
  static Request requestSearchAll = new Request(requestedRoomAmount2, null, null, null, null, false, false, null, null,
          null, null, false, false);
  static Request requestSearchWithLoggia = new Request(requestedRoomAmount2, null, null, null, null, true, false, null,
          null, null, null, false, false);
  static Request requestSearchWithBalcony = new Request(requestedRoomAmount2, 3_000_000, 5_000_000, 10, 100, true, true,
          1, 9, 1, 9, false, false);
  static Request requestSearchNone = new Request(requestedRoomAmount2, null, 500_000, null, null, false, false, null,
          null, null, null, false, false);
  static Request requestSearchNotLastFloor = new Request(requestedRoomAmount2, null, null, null, null, false, false,
          null, null, null, null, false, true);
  static Request requestSearchNotFirstFloor = new Request(requestedRoomAmount2, null, null, null, null, false, false,
          null, null, null, null, true, false);
  @Test
  @Order(1)
  void shouldCreate() {
    ArrayList<Apartment> expectedAll = new ArrayList<>();
    ArrayList<Apartment> actualAll = manager.getAll();
    assertEquals(expectedAll, actualAll);
    int actualCount = manager.getCount();
    assertEquals(0, actualCount);
  }

  @Test
  @Order(2)
  void shouldAddSingle() {
    Apartment actualCreate = manager.create(apartmentFirstFloor);
    ArrayList<Apartment> expectedAll = new ArrayList<>();
    expectedAll.add(apartmentFirstFloor);
    ArrayList<Apartment> actualAll = manager.getAll();
    assertEquals(expectedAll, actualAll);
    Apartment actualById = manager.getById(actualCreate.getId());
    assertNotNull(actualById);

    Integer count = manager.getCount();
    assertNotEquals(0, count);
  }

  @Test
  @Order(3)
  void shouldAddMultiple() {
    ArrayList<Apartment> expectedAll = manager.getAll();

    expectedAll.add(apartmentFiveAndMoreHasLoggia);
    expectedAll.add(apartment3LastFloor);
    manager.create(apartmentFiveAndMoreHasLoggia);
    manager.create(apartment3LastFloor);

    ArrayList<Apartment> actualAll = manager.getAll();
    assertEquals(expectedAll, actualAll);
    assertEquals(expectedAll.size(), manager.getCount());
  }
  @Test
  @Order(4)
  void shouldSearchMultiple() {
    ArrayList<Apartment> expected = new ArrayList<>();
    expected.add(apartmentFiveAndMoreHasLoggia);
    expected.add(apartment3LastFloor);
    ArrayList<Apartment> actual = manager.search(requestSearchMultiple);
    assertEquals(expected, actual);
  }
  @Test
  @Order(5)
  void shouldSearchSingleResult() {
    ArrayList<Apartment> expected = new ArrayList<>();
    expected.add(apartmentFiveAndMoreHasLoggia);
    ArrayList<Apartment> actual = manager.search(requestSearchOne);
    assertEquals(expected, actual);
  }
  @Test
  @Order(6)
  void shouldSearchNoResults() {
    ArrayList<Apartment> actualAll = manager.search(requestSearchNone);
    assertEquals(0, actualAll.size());
  }
  @Test
  @Order(7)
  void shouldShowAll() {
    ArrayList<Apartment> expectedAll = manager.getAll();
    ArrayList<Apartment> actualAll = manager.search(requestSearchAll);
    assertEquals(expectedAll, actualAll);
    assertEquals(expectedAll.size(), actualAll.size());
  }
  @Test
  @Order(8)
  void shouldSearchWithLoggia() {
    ArrayList<Apartment> expected = new ArrayList<>();
    expected.add(apartmentFiveAndMoreHasLoggia);
    expected.add(apartment3LastFloor);
    ArrayList<Apartment> actual = manager.search(requestSearchWithLoggia);
    assertEquals(expected, actual);
  }
  @Test
  @Order(9)
  void shouldSearchWithBalcony() {
    ArrayList<Apartment> expected = new ArrayList<>();
    expected.add(apartment3LastFloor);
    ArrayList<Apartment> actual = manager.search(requestSearchWithBalcony);
    assertEquals(expected, actual);
  }
  @Test
  @Order(10)
  void shouldUpdateExistent() {
    apartmentFiveAndMoreHasLoggia.setPrice(6_000_000);
    Apartment expected = apartmentFiveAndMoreHasLoggia;
    Apartment actual = manager.update(apartmentFiveAndMoreHasLoggia);
    assertEquals(expected, actual);
  }
  @Test
  @Order(11)
  void shouldUpdateNotExistent() {
    assertThrows(ApartmentNotFoundException.class, () -> {
      manager.update(apartmentDoesNotExists);
    });
  }
  @Test
  @Order(12)
  void shouldRemoveExistent() {
    int expectedCount = manager.getCount() - 1;
    manager.removeById(apartmentFirstFloor.getId());
    int actualCount = manager.getCount();
    assertEquals(expectedCount, actualCount);
  }
  @Test
  @Order(13)
  void shouldRemoveNotExistent() {
    assertThrows(ApartmentNotFoundException.class, () -> {
      manager.removeById(apartmentFirstFloor.getId());
    });
  }
  @Test
  @Order(14)
  void shouldSearchNotFirstFloor() {
    ArrayList<Apartment> expected = new ArrayList<>();
    expected.add(apartmentFiveAndMoreHasLoggia);
    expected.add(apartment3LastFloor);
    ArrayList<Apartment> actual = manager.search(requestSearchNotFirstFloor);
    assertEquals(expected.size(), actual.size());
  }
  @Test
  @Order(15)
  void shouldSearchNotLastFloor() {
    manager.create(apartmentFirstFloor);
    ArrayList<Apartment> expected = new ArrayList<>();
    expected.add(apartmentFirstFloor);
    expected.add(apartmentFiveAndMoreHasLoggia);
    ArrayList<Apartment> actual = manager.search(requestSearchNotLastFloor);
    assertEquals(expected.size(), actual.size());
  }
  @Test
  @Order(16)
  void shouldSearchNotExistingById() {
    assertThrows(ApartmentNotFoundException.class, () -> {
      manager.getById(apartmentDoesNotExists.getId());
    });
  }
  @Test
  @Order(17)
  void shouldSearchLimited() {
    int expected = 10;
    int count = expected;
    ArrayList<Apartment> items = manager.getAll();
    int id = items.size();

    while (count>=0) {
      apartmentFiveAndMoreHasLoggia.setId(id);
      manager.create(apartmentFiveAndMoreHasLoggia);
      count--;
      id++;
    }

    ArrayList<Apartment> actual = manager.search(requestSearchOne);
    assertEquals(expected, actual.size());
  }
}
