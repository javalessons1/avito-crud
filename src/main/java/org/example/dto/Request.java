package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Request {
    private String[] roomAmount;
    private Integer minPrice;
    private Integer maxPrice;
    private Integer minSize;
    private Integer maxSize;
    private boolean withLoggia;
    private boolean withBalcony;
    private Integer minFloor;
    private Integer maxFloor;
    private Integer minHomeFloor;
    private Integer maxHomeFloor;
    private boolean isFirstFloor;
    private boolean isLastFloor;
}
