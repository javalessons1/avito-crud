package org.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.dto.Request;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Apartment {
  private long id;
  private String roomAmount;
  private int price;
  private int size;
  private boolean withLoggia;
  private boolean withBalcony;
  private int floor;
  private int floorsInHouse;

  public boolean isFirstFloor(){

    return floor == 1;
  }
  public boolean isLastFloor(){

    return floor==floorsInHouse;
  }

  boolean compareRoomAmounts(String[] roomAmountNeeded) {
    for (String type : roomAmountNeeded) {
      if (roomAmount.equalsIgnoreCase(type)) {
          return true;
      }
    }
    return false;
  }
  public boolean matches(Request request){
    String[] roomAmountNeeded = request.getRoomAmount();
    if (roomAmountNeeded.length > 0) {
      if (!compareRoomAmounts(roomAmountNeeded)) {
          return false;
      }
    }
    if (request.getMinPrice()!=null) {
      if (request.getMinPrice() > price) {
        return false;
      }
    }
    if (request.getMaxPrice()!=null) {
      if (request.getMaxPrice() < price) {
        return false;
      }
    }
    if (request.getMinSize()!=null) {
      if (request.getMinSize()>size) {
        return false;
      }
    }
    if (request.getMaxSize()!=null) {
      if (request.getMaxSize()<size) {
        return false;
      }
    }
    if (request.isWithLoggia()) {
      if (!withLoggia) {
        return false;
      }
    }
    if (request.isWithBalcony()) {
      if (!withBalcony) {
        return false;
      }
    }
    if (request.getMinFloor()!=null) {
      if (request.getMinFloor()>floor) {
        return false;
      }
    }
    if (request.getMaxFloor()!=null) {
      if (request.getMaxFloor()<floor) {
        return false;
      }
    }
    if (request.isFirstFloor()) {
      if (isFirstFloor()) {
        return false;
      }
    }
    if (request.isLastFloor()) {
      if (isLastFloor()) {
        return false;
      }
    }
    if (request.getMinHomeFloor() != null){
      if (request.getMinHomeFloor() > floorsInHouse){
        return false;
      }
    }
    if (request.getMaxHomeFloor() != null){
      if (request.getMaxHomeFloor() < floorsInHouse){
        return false;
      }
    }
    return true;
  }
}
