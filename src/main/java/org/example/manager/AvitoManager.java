package org.example.manager;
import lombok.extern.slf4j.Slf4j;
import org.example.data.Apartment;
import org.example.dto.Request;
import java.util.ArrayList;
import java.util.List;

import org.example.exeption.ApartmentNotFoundException;

@Slf4j
public class AvitoManager {

  public static final int searchSize = 10;
  private List<Apartment> items = new ArrayList<>(9);

  public ArrayList<Apartment> getAll() {
    return new ArrayList<>(items);
  }

  public Apartment getById(long id) {
    for (Apartment item : items) {
      if (item.getId() == id) {
        return item;
      }
    }
    throw new ApartmentNotFoundException("Apartment with id: " + id + " was deleted or never existed");
  }

  public ArrayList<Apartment> search(Request request) {
    List<Apartment> results = new ArrayList<>(searchSize);
    for (Apartment item : items) {
      if (item.matches(request)) {
        results.add(item);
        if (results.size() >= searchSize) {
          return (ArrayList<Apartment>) results;
        }
      }

    }
    log.debug("{} apartments is found", results.size());
    return (ArrayList<Apartment>) results;
  }

  public Apartment create(Apartment item) {
    items.add(item);
    log.debug("Apartment with Id: {} was added", item.getId());
    return item;
  }

  public Apartment update(Apartment item) {
    int index = getIndexById(item.getId());
    if (index == -1) {
      throw new ApartmentNotFoundException("Apartment with such id does not exist");
    }

    items.set(index, item);
    log.debug("Apartment information was changed. ID: {}", item.getId());
    return item;
  }

  public void removeById(long id) {
    int index = getIndexById(id);
    if (index == -1){
      throw new ApartmentNotFoundException("Apartment was deleted or never existed");
    }
    items.remove(index);
    log.debug("Apartment with id {} was removed", id);

  }

  public int getCount() {
    return items.size();
  }

  private int getIndexById(long id) {
    for (int i = 0; i < items.size(); i++) {
      if (items.get(i).getId() == id) {
        return i;
      }
    }
    return -1;
  }
}
